//logo fade in and out
$(document).ready(function () {
  var logoElement = $('.overImage');
  $(window).on('scroll', function () {
    var scroll = $(this).scrollTop();
    logoElement.css({
      'opacity': 1 - scroll / 307
    });
  });
});

//HEADSHOT FADE IN AND OUT
$(document).ready(function () {
  if($('body').is('.indexPage')){
  $(window).on('scroll', function () {
  var headshotID = document.getElementById("headshot");
  var headshotBound = headshotID.getBoundingClientRect();
  // headshotBound.top returns 0 at the top of the headshot, or how many pixels above (+) or below(-) the headshot the scroll is.
  
  var screenHeight = screen.height;
  var scrollPosY = window.pageYOffset || document.documentElement.scrollTop; // Y axis pixel count (from top of page)

  var headTopAtBottom = Math.round(headshotBound.top - screenHeight);  // pixel count for when the top of the headshot hits bottom of page.
  var headTopAtTop = Math.round(headshotBound.top);  // pixel count for when the top of the headshot hits top of page.
  var heightOfImage = $(headshotID).height();
  var bottomOfHeadAtTop = headTopAtTop +heightOfImage;

  var image = $('.headshot');

  var buttons = $('.socialButton');

    if (headTopAtBottom < 0) {
      image.css({
      'opacity': (headTopAtBottom * -1) / (screenHeight/2+100)
    });
      buttons.css({
      'opacity': (headTopAtBottom * -1) / (screenHeight/2)
    });
    }
    if(bottomOfHeadAtTop < screenHeight/2){      
      image.css({
      'opacity': ((bottomOfHeadAtTop+heightOfImage/2) / (screenHeight/2))
    });
      buttons.css({
      'opacity': (headTopAtBottom * -1) / (screenHeight/2)
    });
    }
  });
  }
});
// =================================================== //


// hide and show navbar on scroll
var prevScrollPos = window.pageYOffset;
window.onscroll = function () {
    
  var x = document.getElementById("navBarID");
  x.className = "navBar";

  var currentScrollPos = window.pageYOffset;  
  if (prevScrollPos > currentScrollPos || currentScrollPos < 1 || prevScrollPos < 1){
    document.getElementById("navBarID").style.top = "0";
    document.getElementById("navBarID").style.WebkitTransitionDuration = "0.5s";
  } else {
    document.getElementById("navBarID").style.top = "-50px";

  }
  prevScrollPos = currentScrollPos;
  
};


// SHOWS navbar when scrolling stops.
var timer;
window.addEventListener('scroll', function ( event ) {
	window.clearTimeout(timer);
	timer = setTimeout(function() {
    document.getElementById("navBarID").style.top = "0";
	}, 300);
}, false);


// activate responsive burger menu when clicked.
function responsiveBurger() {
  var x = document.getElementById("navBarID");
  if (x.className === "navBar") {
    x.className += " responsive";
  } else {
    x.className = "navBar";
  }

}

// //FUNCTION TO ENABLE SMOOTH SCROLLING WHEN JUMPING ANCHORS
// $(document).ready(function () {
//   $("a").on('click', function (event) {
//     if (this.hash !== "") {
//       var hash = this.hash;
//       // '1000' specifies ms to scroll
//       $('html, body').animate({
//         scrollTop: $(hash).offset().top
//       }, 1000, function () {
//         window.location.hash = hash;
//       });
//     }
//   });
// });


// SHOW AND HIDE THE VIDEO ON THE GALLERY PAGE
var app = angular.module('myApp', []);
app.controller('myController', function ($scope) {
  $scope.showVideo = false;
  $scope.videoText = 'Show Video';
  $scope.showPhotoVideo = function () {
    if ($scope.videoText === 'Show Video') {
      $scope.videoText = 'Hide Video';
    } else {
      $scope.videoText = 'Show Video';
    }
    $scope.showVideo = !$scope.showVideo;
  };
});

// SHOW AND HIDE THE KIA ORA VIDEO ON THE ABOUT PAGE
$(document).ready(function () {
  var url = $("#kiaOraVideo").attr('src');
  $("#videoModal").on('hide.bs.modal', function () {
    $("#kiaOraVideo").attr('src', '');
  });
  $("#videoModal").on('show.bs.modal', function () {
    $("#kiaOraVideo").attr('src', url);
  });
});